package db

import (
	protobufMain "develop/protobuf/main"
	protobufWallet "develop/protobuf/wallet"
	"time"
)

//go:generate reform

//reform:request
type Request struct {
	Id        int64                     	`reform:"id"`
	WalletId  int64                     	`reform:"wallet_id"`
	Amount    float64                   	`reform:"amount"`
	Type      protobufWallet.RequestType   	`reform:"type"`
	Status    protobufWallet.RequestStatus 	`reform:"status"`
	Source    protobufWallet.RequestSource 	`reform:"source"`
	IpAddr    *string                   	`reform:"ip_addr"`
	IpCountry protobufMain.Country       	`reform:"ip_country"`
	Gateway   protobufWallet.Gateway       	`reform:"gateway"`
	Created   *time.Time                	`reform:"created"`
	Updated   *time.Time                	`reform:"updated"`
}
