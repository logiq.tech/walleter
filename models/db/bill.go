package db

import (
	protobufMain "develop/protobuf/main"
	protobufWallet "develop/protobuf/wallet"
	"time"
)

//go:generate reform

//reform:bill
type Bill struct {
	Id           int64                   	`reform:"id"`
	RequestId    int64                   	`reform:"request_id"`
	Type         protobufWallet.BillType    `reform:"type"`
	Status       protobufWallet.BillStatus  `reform:"status"`
	Amount       float64                 	`reform:"amount"`
	Gateway      protobufWallet.Gateway     `reform:"gateway"`
	ReferenceId  *string                 	`reform:"reference_id"`
	Problem      protobufWallet.BillProblem `reform:"problem"`
	WithdrawId   *int64                  	`reform:"withdraw_id"`
	Mask         *string                 	`reform:"mask"`
	Country      protobufMain.Country     	`reform:"country"`
	ExchangeRate *float64                	`reform:"exchange_rate"`
	Created      *time.Time              	`reform:"created"`
	Updated      *time.Time              	`reform:"updated"`
}
