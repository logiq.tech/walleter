package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"
	"develop/main"
	"develop/wallet/models/db"
	"math/rand"
	"develop/queue"
	"github.com/jackc/pgx/pgtype"
	"strconv"
)

func testWithdraw(client protobufWallet.WalleterClient) {
	Describe("WithdrawRequest()", func() {
		Context("Correct request", func() {
			It("should save and return Request", func() {
				dbWallet := createWallet(testWalletUserId)
				amount := rand.Float64()
				purseId := randInt64()
				gateway := protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))

				defer deleteWallet(dbWallet.Id)

				protoRequest, err := client.WithdrawRequest(main.Ctx(), &protobufWallet.Request{
					Wallet:   &protobufWallet.Wallet{Id: dbWallet.Id},
					Amount:    amount,
					Purse:     &protobufWallet.Purse{Id: purseId},
					Gateway:   gateway,
				})
				Ω(err).Should(BeNil())

				compareWallet(protoRequest.Wallet, dbWallet)

				Ω(protoRequest.Purse.Id).To(Equal(purseId))
				Ω(protoRequest.Type).To(Equal(protobufWallet.RequestTypeWithdraw))
				Ω(protoRequest.Status).To(Equal(protobufWallet.RequestStatusCreated))
				Ω(protoRequest.Amount).To(Equal(amount))
				Ω(protoRequest.Gateway).To(Equal(gateway))

				dbRequest := new(db.Request)
				Ω(dbRequest.FindById(protoRequest.Id)).Should(BeNil())

				compareRequest(protoRequest, dbRequest, dbWallet, []*db.Bill{})
			})
		})
	})

	Describe("ConductRequest()", func() {
		Context("correct request", func() {
			It("should change Request status to conducted and create hold and balance transaction", func() {
				dbWallet := createWallet(testWalletUserId)
				dbWallet.Balance = 100
				dbWallet.Save()
				dbRequest := createRequest(dbWallet.Id)
				dbRequest.Amount = 100
				dbRequest.Save()

				balanceBefore := dbWallet.Balance

				defer deleteWallet(dbWallet.Id)

				protoRequest, err := client.ConductRequest(main.Ctx(), &protobufMain.ById{
					Id:   dbRequest.Id,
				})
				Ω(err).Should(BeNil())

				dbWalletFromDb := new(db.Wallet)
				Ω(dbWalletFromDb.FindById(dbWallet.Id)).Should(BeNil())

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())

				dbRequest.Status = protobufWallet.RequestStatusConducted
				dbRequest.Updated = dbRequestFromDb.Updated
				dbWallet.Updated = dbWalletFromDb.Updated
				dbWallet.Balance = dbWallet.Balance - dbRequest.Amount
				compareRequest(protoRequest, dbRequest, dbWallet, []*db.Bill{})

				dbTransactionHoldFromDb := new(db.Transaction)
				Ω(dbTransactionHoldFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
					protobufWallet.TransactionReferenceRequest, dbRequest.Id)).Should(BeNil())

				Ω(dbTransactionHoldFromDb.WalletId).To(Equal(dbWallet.Id))
				Ω(dbTransactionHoldFromDb.Type).To(Equal(protobufWallet.TransactionTypeWithdraw))
				Ω(dbTransactionHoldFromDb.Direction).To(Equal(protobufWallet.TransactionDirectionIncrease))
				Ω(dbTransactionHoldFromDb.Before).To(Equal(0.0))
				Ω(dbTransactionHoldFromDb.Amount).To(Equal(dbRequest.Amount))
				Ω(dbTransactionHoldFromDb.After).To(Equal(dbRequest.Amount))

				dbTransactionBonusFromDb := new(db.Transaction)
				Ω(dbTransactionBonusFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetBalance,
					protobufWallet.TransactionReferenceRequest, dbRequest.Id)).Should(BeNil())

				Ω(dbTransactionBonusFromDb.WalletId).To(Equal(dbWallet.Id))
				Ω(dbTransactionBonusFromDb.Type).To(Equal(protobufWallet.TransactionTypeWithdraw))
				Ω(dbTransactionBonusFromDb.Direction).To(Equal(protobufWallet.TransactionDirectionDecrease))
				Ω(dbTransactionBonusFromDb.Before).To(Equal(balanceBefore))
				Ω(dbTransactionBonusFromDb.Amount).To(Equal(dbRequest.Amount))
				Ω(dbTransactionBonusFromDb.After).To(Equal(balanceBefore - dbRequest.Amount))
			})
		})
	})

	Describe("ProgressWithdraw()", func() {
		Context("correct request with and without Reference and problem", func() {
			It("should create bill without reference and problem, save it to db and save to queue", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)

				dbRequest := createRequest(dbWallet.Id)

				randGateway := protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))

				main.Logger().Infof("randGateway: %s, randGateway: %[1]d", randGateway)

				protoBill1 := &protobufWallet.Bill{
					Request: &protobufWallet.Request{Id: dbRequest.Id},
					Amount: rand.Float64(),
					Gateway: randGateway,
				}

				protoBill2 := &protobufWallet.Bill{
					Request: &protobufWallet.Request{Id: dbRequest.Id},
					Amount: rand.Float64(),
					Gateway: protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
					ReferenceId: &protobufMain.String{Value:randStringRunes(255)},
					Problem: protobufWallet.BillProblem(randIntMapKey(protobufWallet.BillProblem_name)),
				}

				subscriber, err := queue.NewSubscriber(queue.Queue{
					Name:     "testing.bill.withdraw",
					Exchange: queue.EventInExchange,
					Key:      queue.EventKey("bill.withdraw"),
				})
				Ω(err).Should(BeNil())
				events := subscriber.Subscribe(main.Ctx())

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill1)
				Ω(err).Should(BeNil())

				dbBill := new(db.Bill)
				Ω(dbBill.FindLastByWalletAndType(protobufWallet.BillTypeWithdraw, dbWallet.Id)).Should(BeNil())

				Ω(dbBill.RequestId).Should(Equal(dbRequest.Id))
				Ω(dbBill.Amount).Should(Equal(protoBill1.Amount))
				Ω(dbBill.Gateway).Should(Equal(protoBill1.Gateway))
				Ω(dbBill.Type).Should(Equal(protobufWallet.BillTypeWithdraw))
				Ω(dbBill.Status).Should(Equal(protobufWallet.BillStatusProgressed))

				event := <- events
				protoBillFromQueue := new(protobufWallet.Bill)
				Ω(protoBillFromQueue.Unmarshal(event.Delivery.Body)).Should(BeNil())
				event.Delivery.Ack(false)

				Ω(protoBillFromQueue.Request.Id).Should(Equal(dbRequest.Id))
				Ω(protoBillFromQueue.Amount).Should(Equal(protoBill1.Amount))
				Ω(protoBillFromQueue.Gateway).Should(Equal(protoBill1.Gateway))
				Ω(protoBillFromQueue.Type).Should(Equal(protobufWallet.BillTypeWithdraw))
				Ω(protoBillFromQueue.Status).Should(Equal(protobufWallet.BillStatusProgressed))

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill2)
				Ω(err).Should(BeNil())

				dbBill2 := new(db.Bill)
				Ω(dbBill2.FindLastByWalletAndType(protobufWallet.BillTypeWithdraw, dbWallet.Id)).Should(BeNil())

				Ω(dbBill2.RequestId).Should(Equal(dbRequest.Id))
				Ω(dbBill2.Amount).Should(Equal(protoBill2.Amount))
				Ω(dbBill2.Gateway).Should(Equal(protoBill2.Gateway))
				Ω(dbBill2.Type).Should(Equal(protobufWallet.BillTypeWithdraw))
				Ω(dbBill2.Status).Should(Equal(protobufWallet.BillStatusProgressed))
				Ω(dbBill2.ReferenceId.String).Should(Equal(protoBill2.ReferenceId.Value))
				Ω(dbBill2.Problem).Should(Equal(protoBill2.Problem))

				event2 := <- events
				protoBillFromQueue2 := new(protobufWallet.Bill)
				Ω(protoBillFromQueue2.Unmarshal(event2.Delivery.Body)).Should(BeNil())
				event2.Delivery.Ack(false)

				Ω(protoBillFromQueue2.Request.Id).Should(Equal(dbRequest.Id))
				Ω(protoBillFromQueue2.Amount).Should(Equal(protoBill2.Amount))
				Ω(protoBillFromQueue2.Gateway).Should(Equal(protoBill2.Gateway))
				Ω(protoBillFromQueue2.Type).Should(Equal(protobufWallet.BillTypeWithdraw))
				Ω(protoBillFromQueue2.Status).Should(Equal(protobufWallet.BillStatusProgressed))
				Ω(protoBillFromQueue2.ReferenceId.Value).Should(Equal(protoBill2.ReferenceId.Value))
				Ω(protoBillFromQueue2.Problem).Should(Equal(protoBill2.Problem))

				subscriber.Remove()
			})
		})
		Context("incorrect input request", func() {
			It("should return error", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)

				protoBill1 := &protobufWallet.Bill{
					Request: &protobufWallet.Request{Id: dbRequest.Id},
				}
				protoBill2 := &protobufWallet.Bill{
					Amount: rand.Float64(),
				}
				protoBill3 := &protobufWallet.Bill{
					Gateway: protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
				}
				protoBill4 := &protobufWallet.Bill{
					Amount: rand.Float64(),
					Gateway: protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
				}
				protoBill5 := &protobufWallet.Bill{
					Request: &protobufWallet.Request{Id: dbRequest.Id},
					Gateway: protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name)),
				}
				defer deleteWallet(dbWallet.Id)

				_, err := client.ProgressWithdraw(main.Ctx(), protoBill1)
				Ω(err).ShouldNot(BeNil())

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill2)
				Ω(err).ShouldNot(BeNil())

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill3)
				Ω(err).ShouldNot(BeNil())

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill4)
				Ω(err).ShouldNot(BeNil())

				_, err = client.ProgressWithdraw(main.Ctx(), protoBill5)
				Ω(err).ShouldNot(BeNil())

			})
		})
	})

	Describe("AcceptWithdraw()", func() {
		Context("Correct request without ReferenceId", func() {
			It("should create bill, refresh request status to Accepted and create Hold Transaction", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)

				protoBill := &protobufWallet.Bill{
					Request: &protobufWallet.Request{Id: dbRequest.Id},
					Amount: rand.Float64(),
				}

				defer deleteWallet(dbWallet.Id)

				_, err := client.AcceptWithdraw(main.Ctx(), protoBill)
				Ω(err).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindLastByWalletAndType(protobufWallet.BillTypeWithdraw, dbWallet.Id)).Should(BeNil())

				Ω(dbBillFromDb.RequestId).To(Equal(dbRequest.Id))
				Ω(dbBillFromDb.Amount).To(Equal(protoBill.Amount))
				Ω(dbBillFromDb.ReferenceId.Status).To(Equal(pgtype.Null))
				Ω(dbBillFromDb.Type).To(Equal(protobufWallet.BillTypeWithdraw))
				Ω(dbBillFromDb.Status).To(Equal(protobufWallet.BillStatusAccepted))

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())
				Ω(dbRequestFromDb.Status).To(Equal(protobufWallet.RequestStatusAccepted))

				dbTransactionHoldFromDb := new(db.Transaction)
				Ω(dbTransactionHoldFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetHold,
					protobufWallet.TransactionReferenceBill, dbBillFromDb.Id)).Should(BeNil())

				Ω(dbTransactionHoldFromDb.WalletId).To(Equal(dbWallet.Id))
				Ω(dbTransactionHoldFromDb.Type).To(Equal(protobufWallet.TransactionTypeWithdraw))
				Ω(dbTransactionHoldFromDb.Direction).To(Equal(protobufWallet.TransactionDirectionDecrease))
				Ω(dbTransactionHoldFromDb.Before).To(Equal(protoBill.Amount))
				Ω(dbTransactionHoldFromDb.Amount).To(Equal(protoBill.Amount))
				Ω(dbTransactionHoldFromDb.After).To(Equal(0.0))
				Ω(dbTransactionHoldFromDb.Reference).To(Equal(protobufWallet.TransactionReferenceBill))
				Ω(dbTransactionHoldFromDb.ReferenceId).To(Equal(strconv.FormatInt(dbBillFromDb.Id, 10)))
			})
		})
	})

	Describe("DeclineWithdraw()", func() {
		Context("Correct request", func() {
			It("should create bill, refresh request status to Decline", func() {

				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				billProblem := protobufWallet.BillProblem(randIntMapKey(protobufWallet.BillProblem_name))
				billReferenceId := &protobufMain.String{ Value: randStringRunes(10)}

				defer deleteWallet(dbWallet.Id)

				protoBill := &protobufWallet.Bill{
					Request: &protobufWallet.Request{ Id: dbRequest.Id },
					Problem: billProblem,
					ReferenceId: billReferenceId,
				}

				_, err := client.DeclineWithdraw(main.Ctx(), protoBill)
				Ω(err).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindLastByWalletAndType(protobufWallet.BillTypeWithdraw, dbWallet.Id)).Should(BeNil())

				Ω(dbBillFromDb.RequestId).To(Equal(dbRequest.Id))
				Ω(dbBillFromDb.Amount).To(Equal(protoBill.Amount))
				Ω(dbBillFromDb.Type).To(Equal(protobufWallet.BillTypeWithdraw))
				Ω(dbBillFromDb.Status).To(Equal(protobufWallet.BillStatusDeclined))
				Ω(dbBillFromDb.Problem).To(Equal(billProblem))
				Ω(dbBillFromDb.ReferenceId.String).To(Equal(billReferenceId.Value))


				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())

				dbRequest.Status = protobufWallet.RequestStatusDeclined
				dbRequest.Updated = dbRequestFromDb.Updated

				compareRequest(dbRequestFromDb.ToProtoWithBills(), dbRequest, dbWallet, []*db.Bill{dbBillFromDb})
			})
		})
	})
}