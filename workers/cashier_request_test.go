package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"

	"develop/main"
	"develop/wallet/models/db"
	"strconv"
	"github.com/jackc/pgx/pgtype"
)

func testRequest(client protobufWallet.WalleterClient) {
	Describe("GetRequest()", func() {
		Context("correct request", func() {
			It("should return request", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				dbBills := []*db.Bill{
					createBill(dbRequest.Id),
					createBill(dbRequest.Id),
				}
				defer deleteWallet(dbWallet.Id)

				protoRequest, err := client.GetRequest(main.Ctx(), &protobufMain.ById{
					Id: dbRequest.Id,
				})
				Ω(err).Should(BeNil())

				compareRequest(protoRequest, dbRequest, dbWallet, dbBills)
			})
		})
	})

	Describe("GetRequestWithHold()", func() {
		Context("correct request", func() {
			It("should return request with hold", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)

				dbBills := []*db.Bill{
					createBill(dbRequest.Id),
					createBill(dbRequest.Id),
				}

				dbTransaction := createTransaction(dbWallet.Id)
				dbTransaction.Target = protobufWallet.TransactionTargetHold
				dbTransaction.Reference = protobufWallet.TransactionReferenceRequest
				dbTransaction.ReferenceId = strconv.FormatInt(dbRequest.Id, 10)
				dbTransaction.Save()

				defer deleteWallet(dbWallet.Id)

				protoRequestWithTransactionHoldReply, err := client.GetRequestWithHold(main.Ctx(), &protobufMain.ById{
					Id: dbRequest.Id,
				})
				Ω(err).Should(BeNil())

				compareRequest(protoRequestWithTransactionHoldReply.Request, dbRequest, dbWallet, dbBills)
				compareTransaction(protoRequestWithTransactionHoldReply.Transaction, dbTransaction, dbWallet)
			})
		})
	})

	Describe("IsCouponUsed()", func() {
		Context("correct request", func() {
			It("should return true", func() {
				dbWallet := createWallet(testWalletUserId)
				createRequest(dbWallet.Id)
				createRequest(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)
				dbRequest.CouponId = &pgtype.Int8{Int: randInt64(), Status: pgtype.Present}
				dbRequest.UseCoupon = &pgtype.Bool{Bool: true, Status: pgtype.Present}
				dbRequest.Save()

				defer deleteWallet(dbWallet.Id)

				protoBoolReply, err := client.IsCouponUsed(main.Ctx(), &protobufWallet.ByWalletAndCoupon{
					Wallet: dbWallet.Id,
					Coupon: dbRequest.CouponId.Int,
				})
				Ω(err).Should(BeNil())

				Ω(protoBoolReply.Value).Should(Equal(true))
			})
		})
		Context("correct request", func() {
			It("should return false", func() {
				dbWallet := createWallet(testWalletUserId)
				createRequest(dbWallet.Id)
				createRequest(dbWallet.Id)
				dbRequest := createRequest(dbWallet.Id)
				dbRequest.CouponId = &pgtype.Int8{Int: randInt64(), Status: pgtype.Present}
				dbRequest.UseCoupon = &pgtype.Bool{Bool: false, Status: pgtype.Present}

				defer deleteWallet(dbWallet.Id)

				protoBoolReply, err := client.IsCouponUsed(main.Ctx(), &protobufWallet.ByWalletAndCoupon{
					Wallet: dbWallet.Id,
					Coupon: dbRequest.CouponId.Int,
				})
				Ω(err).Should(BeNil())

				Ω(protoBoolReply.Value).Should(Equal(false))
			})
		})
	})
}
