package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	protobufWallet "develop/protobuf/wallet"

	"develop/main"
	"develop/queue"
	"develop/main"
)

func testTrading(client protobufWallet.WalleterClient) {
	Describe("Bet And Win", func() {
		Context("correct request", func() {
			It("must fulfill requests Bet and Win", func() {
				main.Logger().Info("Bet And Win")

				sub, err := queue.NewSubscriber(queue.Queue{
					Name:      queue.ServiceQueue("test.bet.and.win"),
					Exchange:  queue.EventInExchange,
					Key:       queue.EventKey(queue.TransactionSaveKey),
					Durable:   true,
				})
				Ω(err).Should(BeNil())
				defer sub.Remove()

				wallet := createWallet(testWalletUserId)
				wallet.Balance = 200
				Ω(wallet.Save()).Should(BeNil())

				defer deleteWallet(wallet.Id)
				amount := 200.0

				main.Logger().Printf("wallet: %v", wallet)

				_, err = client.Bet(main.Ctx(), &protobufWallet.Transaction{
					Wallet: &protobufWallet.Wallet{Id: wallet.Id},
					ReferenceId: "555",
					Amount: amount,
				})
				Ω(err).Should(BeNil())


				events := sub.Subscribe(main.Ctx())

				event1 := <- events
				transactionBetProto := new(protobufWallet.Transaction)
				transactionBetProto.Unmarshal(event1.Delivery.Body)

				Ω(transactionBetProto.Direction).To(Equal(protobufWallet.TransactionDirectionDecrease))
				Ω(transactionBetProto.Reference).To(Equal(protobufWallet.TransactionReferenceOption))
				Ω(transactionBetProto.Type).To(Equal(protobufWallet.TransactionTypeTrading))
				Ω(transactionBetProto.Target).To(Equal(protobufWallet.TransactionTargetBalance))
				Ω(transactionBetProto.Amount).To(Equal(amount))
				event1.Delivery.Ack(true)

				amount = 700.0

				_, err = client.Win(main.Ctx(), &protobufWallet.Transaction{
					Wallet: &protobufWallet.Wallet{Id: wallet.Id},
					ReferenceId: "555",
					Amount: amount,
				})
				Ω(err).Should(BeNil())

				event2 := <- events

				transactionWinProto := new(protobufWallet.Transaction)
				transactionWinProto.Unmarshal(event2.Delivery.Body)

				Ω(transactionWinProto.Direction).To(Equal(protobufWallet.TransactionDirectionIncrease))
				Ω(transactionWinProto.Reference).To(Equal(protobufWallet.TransactionReferenceOption))
				Ω(transactionWinProto.Type).To(Equal(protobufWallet.TransactionTypeTrading))
				Ω(transactionWinProto.Target).To(Equal(protobufWallet.TransactionTargetBalance))
				Ω(transactionWinProto.Amount).To(Equal(amount))
				event2.Delivery.Ack(true)

				amount = 0.0

				_, err = client.Win(main.Ctx(), &protobufWallet.Transaction{
					Wallet: &protobufWallet.Wallet{Id: wallet.Id},
					ReferenceId: "555",
					Amount: amount,
				})
				Ω(err).Should(BeNil())

				event3 := <- events
				transactionWinNoChangeProto := new(protobufWallet.Transaction)
				transactionWinNoChangeProto.Unmarshal(event3.Delivery.Body)

				Ω(transactionWinNoChangeProto.Direction).To(Equal(protobufWallet.TransactionDirectionNoChange))
				Ω(transactionWinNoChangeProto.Reference).To(Equal(protobufWallet.TransactionReferenceOption))
				Ω(transactionWinNoChangeProto.Type).To(Equal(protobufWallet.TransactionTypeTrading))
				Ω(transactionWinNoChangeProto.Target).To(Equal(protobufWallet.TransactionTargetBalance))
				Ω(transactionWinNoChangeProto.Amount).To(Equal(amount))

				event3.Delivery.Ack(true)
			})
		})
		Context("incorrect request: amount more than balance", func() {
			It("should return error about insufficient funds", func() {
				wallet := createWallet(testWalletUserId)
				defer deleteWallet(wallet.Id)
				amount := 2000.0

				main.Logger().Printf("wallet: %v", wallet)

				_, err := client.Bet(main.Ctx(), &protobufWallet.Transaction{
					Wallet: &protobufWallet.Wallet{Id: wallet.Id},
					ReferenceId: "555",
					Amount: amount,
				})
				Ω(err).ShouldNot(BeNil())
			})
		})
	})
}
