package workers_test

import (
	"google.golang.org/grpc"

	. "github.com/onsi/ginkgo"
	protobufWallet "develop/protobuf/wallet"

	"develop/wallet/workers"

)

const (
	testWalletUserId int64 = 999999999
)

var _ = Describe("Walleter", func() {
	appcfg := &app.Config{
		Server: &app.Server{
			Host: "127.0.0.1",
			Port: "5555",
		},
		Amqp: &app.Amqp{
			Host:     "127.0.0.1",
			Port:     5672,
			Vhost:    "",
			User:     "guest",
			Password: "guest",
		},
		DB: &app.Db{
			Host: "127.0.0.1",
			Port: 5432,
			Name: "wallet",
			User: "postgres",
			Pass: "",
		},
		Service: map[string]string{
			"operator": "localhost:5555",
		},
	}
	InitApp("operator-test", appcfg)

	go new(workers.Walleter).Start(appcfg)

	deleteWalletsByUserId(testWalletUserId)

	conn, dialErr := grpc.Dial(appcfg.GetClient()["operator"], grpc.WithInsecure())
	if dialErr != nil {
		Fail("Can't dial to GRPC!")
	}
	client := protobufWallet.NewWalleterClient(conn)

	testWallet(client)
	testRequest(client)

	testDeposit(client)
	testBill(client)

	testWithdraw(client)
	testRefund(client)

	testTrading(client)
	testBalanceAndBonus(client)
})
