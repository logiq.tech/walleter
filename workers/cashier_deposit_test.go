package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"

	"develop/wallet/models/db"
	"math/rand"
	"github.com/jackc/pgx/pgtype"
	"develop/main"
)

func testDeposit(client protobufWallet.WalleterClient) {
	Describe("DepositRequest()", func() {
		Context("correct request", func() {
			It("should create request and bill", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)

				dbRequestNotSaved := new(db.Request)
				dbRequestNotSaved.WalletId = dbWallet.Id

				dbRequestNotSaved.Source = protobufWallet.RequestSource(randIntMapKey(protobufWallet.RequestSource_name))

				dbRequestNotSaved.Amount = rand.Float64()
				dbRequestNotSaved.Bonus = &pgtype.Float8{Float: rand.Float64(), Status: pgtype.Present}
				dbRequestNotSaved.Turnover = &pgtype.Float8{Float: rand.Float64(), Status: pgtype.Present}
				dbRequestNotSaved.UseCoupon = &pgtype.Bool{Bool: randBool(), Status: pgtype.Present}
				dbRequestNotSaved.CouponId = &pgtype.Int8{Int: randInt64(), Status: pgtype.Present}
				dbRequestNotSaved.UseRate = &pgtype.Bool{Bool: randBool(), Status: pgtype.Present}
				dbRequestNotSaved.GaCid = &pgtype.Varchar{String: randStringRunes(255), Status: pgtype.Present}
				dbRequestNotSaved.IpAddr = &pgtype.Varchar{String: randStringRunes(16), Status: pgtype.Present}
				dbRequestNotSaved.IpCountry = protobufMain.Country(randIntMapKey(protobufMain.Country_name))

				dbRequestNotSaved.Gateway = protobufWallet.Gateway(randIntMapKey(protobufWallet.Gateway_name))

				dbRequestNotSaved.Type = protobufWallet.RequestTypeDeposit
				dbRequestNotSaved.Status = protobufWallet.RequestStatusCreated

				protoBillReply, err := client.DepositRequest(main.Ctx(), dbRequestNotSaved.ToProto())
				Ω(err).Should(BeNil())

				dbRequestFormDb := new(db.Request)
				Ω(dbRequestFormDb.FindById(protoBillReply.Request.Id)).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(protoBillReply.Id)).Should(BeNil())

				compareBill(protoBillReply, dbBillFromDb, dbRequestFormDb, dbWallet, true)

				Ω(protoBillReply.Request.Wallet.Id).To(Equal(dbRequestNotSaved.WalletId))
				Ω(protoBillReply.Request.Source).To(Equal(dbRequestNotSaved.Source))
				Ω(protoBillReply.Request.Amount).To(Equal(dbRequestNotSaved.Amount))
				Ω(protoBillReply.Request.Bonus.Value).To(Equal(dbRequestNotSaved.Bonus.Float))
				Ω(protoBillReply.Request.Turnover.Value).To(Equal(dbRequestNotSaved.Turnover.Float))
				Ω(protoBillReply.Request.UseCoupon.Value).To(Equal(dbRequestNotSaved.UseCoupon.Bool))
				Ω(protoBillReply.Request.CouponId.Value).To(Equal(dbRequestNotSaved.CouponId.Int))
				Ω(protoBillReply.Request.UseRate.Value).To(Equal(dbRequestNotSaved.UseRate.Bool))
				Ω(protoBillReply.Request.GaCid.Value).To(Equal(dbRequestNotSaved.GaCid.String))
				Ω(protoBillReply.Request.IpAddr.Value).To(Equal(dbRequestNotSaved.IpAddr.String))
				Ω(protoBillReply.Request.IpCountry).To(Equal(dbRequestNotSaved.IpCountry))
				Ω(protoBillReply.Request.Gateway).To(Equal(dbRequestNotSaved.Gateway))
				Ω(protoBillReply.Request.Type).To(Equal(dbRequestNotSaved.Type))
				Ω(protoBillReply.Request.Status).To(Equal(dbRequestNotSaved.Status))

				Ω(protoBillReply.Type).To(Equal(protobufWallet.BillTypeDeposit))
				Ω(protoBillReply.Status).To(Equal(protobufWallet.BillStatusConducted))
				Ω(protoBillReply.Amount).To(Equal(dbRequestNotSaved.Amount))
				Ω(protoBillReply.Gateway).To(Equal(dbRequestNotSaved.Gateway))
			})
		})
	})

	Describe("AcceptDeposit()", func() {
		Context("correct request with bonus", func() {
			It("should change bill status, change request status, create deposit transaction and create bonus transaction if exist bonus", func() {
				dbWallet := createWallet(testWalletUserId)
				var balanceBefore float64 = 1000
				dbWallet.Balance = balanceBefore
				Ω(dbWallet.Save()).Should(BeNil())

				dbRequest := createRequest(dbWallet.Id)
				dbRequest.Amount = 100
				dbRequest.Bonus = &pgtype.Float8{Float: 1000, Status: pgtype.Present}
				Ω(dbRequest.Save()).Should(BeNil())

				dbBill := createBill(dbRequest.Id)

				defer deleteWallet(dbWallet.Id)

				protoRequestReply, err := client.AcceptDeposit(main.Ctx(), dbBill.ToProtoWithRequestProto())
				Ω(err).Should(BeNil())

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())
				dbRequest.Status = protobufWallet.RequestStatusAccepted
				dbRequest.Updated = dbRequestFromDb.Updated

				dbWalletFromDb := new(db.Wallet)
				Ω(dbWalletFromDb.FindById(dbWallet.Id)).Should(BeNil())
				dbWallet.Locked = dbWalletFromDb.Locked
				dbWallet.Balance = dbWallet.Balance + dbRequest.Amount + dbRequest.Bonus.Float

				dbWallet.Updated = dbWalletFromDb.Updated

				dbBill.Status = protobufWallet.BillStatusAccepted
				compareRequest(protoRequestReply, dbRequest, dbWallet, []*db.Bill{dbBill})

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(dbBill.Id)).Should(BeNil())

				dbBill.Status = protobufWallet.BillStatusAccepted
				dbBill.Updated = dbBillFromDb.Updated
				compareBill(dbBillFromDb.ToProtoWithRequestProto(), dbBill, dbRequest, dbWallet, true)

				dbTransactionBalanceFromDb := new(db.Transaction)
				Ω(dbTransactionBalanceFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetBalance,
					protobufWallet.TransactionReferenceBill, dbBill.Id)).Should(BeNil())

				Ω(dbTransactionBalanceFromDb.WalletId).To(Equal(dbWallet.Id))
				Ω(dbTransactionBalanceFromDb.Type).To(Equal(protobufWallet.TransactionTypeDeposit))
				Ω(dbTransactionBalanceFromDb.Direction).To(Equal(protobufWallet.TransactionDirectionIncrease))
				Ω(dbTransactionBalanceFromDb.Before).To(Equal(balanceBefore))
				Ω(dbTransactionBalanceFromDb.Amount).To(Equal(dbRequest.Amount))
				Ω(dbTransactionBalanceFromDb.After).To(Equal(balanceBefore + dbRequest.Amount))

				dbTransactionBonusFromDb := new(db.Transaction)
				Ω(dbTransactionBonusFromDb.FindLastByTargetAndReference(protobufWallet.TransactionTargetBonus,
					protobufWallet.TransactionReferenceBill, dbBill.Id)).Should(BeNil())

				Ω(dbTransactionBonusFromDb.WalletId).To(Equal(dbWallet.Id))
				Ω(dbTransactionBonusFromDb.Type).To(Equal(protobufWallet.TransactionTypeDeposit))
				Ω(dbTransactionBonusFromDb.Direction).To(Equal(protobufWallet.TransactionDirectionIncrease))
				Ω(dbTransactionBonusFromDb.Before).To(Equal(dbTransactionBalanceFromDb.After))
				Ω(dbTransactionBonusFromDb.Amount).To(Equal(dbRequest.Bonus.Float))
				Ω(dbTransactionBonusFromDb.After).To(Equal(dbWallet.Balance))
			})
		})
	})

	Describe("DeclineDeposit()", func() {
		Context("correct request", func() {
			It("should change status of bill and request to 'Declined'", func() {
				dbWallet := createWallet(testWalletUserId)
				dbRequest := createRequest(dbWallet.Id)
				dbBill := createBill(dbRequest.Id)

				defer deleteWallet(dbWallet.Id)

				protoRequestReply, err := client.DeclineDeposit(main.Ctx(), dbBill.ToProtoWithRequestId())
				Ω(err).Should(BeNil())

				dbRequest.Status = protobufWallet.RequestStatusDeclined
				dbBill.Status = protobufWallet.BillStatusDeclined

				dbRequestFromDb := new(db.Request)
				Ω(dbRequestFromDb.FindById(dbRequest.Id)).Should(BeNil())

				dbBillFromDb := new(db.Bill)
				Ω(dbBillFromDb.FindById(dbBill.Id)).Should(BeNil())

				dbWalletFromDb := new(db.Wallet)
				Ω(dbWalletFromDb.FindById(dbWallet.Id)).Should(BeNil())

				dbRequest.Updated = dbRequestFromDb.Updated
				dbBill.Updated = dbBillFromDb.Updated
				compareRequest(protoRequestReply, dbRequest, dbWallet, []*db.Bill{dbBill})

				compareRequest(protoRequestReply, dbRequestFromDb, dbWalletFromDb, []*db.Bill{dbBillFromDb})
			})

		})
	})

	Describe("CountDepositRequestsByWallet()", func() {
		Context("correct request", func() {
			It("should count deposit requests by walletId", func() {
				dbWallet := createWallet(testWalletUserId)
				numDepositAccepted := 3
				for i := 0; i <= numDepositAccepted + 1; i++ {
					dbRequest := createRequest(dbWallet.Id)
					if i == numDepositAccepted {
						dbRequest.Type = protobufWallet.RequestTypeWithdraw
					} else {
						dbRequest.Type = protobufWallet.RequestTypeDeposit
					}
					if i == numDepositAccepted + 1 {
						dbRequest.Status = protobufWallet.RequestStatusDeclined
					} else {
						dbRequest.Status = protobufWallet.RequestStatusAccepted
					}
					dbRequest.Save()
				}

				defer deleteWallet(dbWallet.Id)

				protoCount, err := client.CountDepositRequestsByWallet(main.Ctx(), &protobufMain.ById{
					Id: dbWallet.Id,
				})
				Ω(err).Should(BeNil())

				Ω(protoCount.Count).Should(Equal(int64(numDepositAccepted)))
			})

		})
	})
}
