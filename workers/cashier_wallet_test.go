package workers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"develop/protobuf/wallet"

	protobufWallet "develop/protobuf/wallet"
	protobufMain "develop/protobuf/main"

	"develop/wallet/models/db"
	"develop/main"
)

// todo придумать генерацию корректного юзерИд

func testWallet(client wallet.WalleterClient) {
	Describe("CreateWallet()", func() {
		Context("correct request", func() {
			It("should create wallet demo in db", func() {
				walletCurrency := protobufMain.CurrencyAED
				walletType := protobufWallet.WalletTypeDemo

				protobufWallet, err := client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					User:     testWalletUserId,
					Currency: walletCurrency,
					Type:     walletType,
				})
				Ω(err).Should(BeNil())

				defer deleteWallet(protobufWallet.Id)

				dbWallet := new(db.Wallet)
				err = dbWallet.FindByUserIdAndType(testWalletUserId, walletType)
				Ω(err).Should(BeNil())
				Ω(dbWallet.Balance).Should(Equal(float64(1000)))

				compareWallet(protobufWallet, dbWallet)
			})
		})
		Context("correct request", func() {
			It("should create wallet real in db", func() {
				walletCurrency := protobufMain.CurrencyAED
				walletType := protobufWallet.WalletTypeReal

				protobufWallet, err := client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					User:     testWalletUserId,
					Currency: walletCurrency,
					Type:     walletType,
				})
				Ω(err).Should(BeNil())

				defer deleteWallet(protobufWallet.Id)

				dbWallet := new(db.Wallet)
				err = dbWallet.FindByUserIdAndType(testWalletUserId, walletType)
				Ω(err).Should(BeNil())
				Ω(dbWallet.Balance).Should(Equal(float64(0)))

				compareWallet(protobufWallet, dbWallet)
			})
		})
		Context("incorrect request: wallet already exist", func() {
			It("should return error that wallet already exist", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)
				_, err := client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					User:     dbWallet.UserId,
					Currency: dbWallet.Currency,
					Type:     dbWallet.Type,
				})
				Ω(err).ShouldNot(BeNil())
			})
		})
		Context("incorrect request: missing required parameter", func() {
			It("should return error that missing required parameter", func() {
				_, err := client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					Currency: 2,
					Type:     2,
				})
				Ω(err).ShouldNot(BeNil())
				_, err = client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					User:     2,
					Currency: 2,
				})
				Ω(err).ShouldNot(BeNil())
				_, err = client.CreateWallet(main.Ctx(), &protobufWallet.WalletRequest{
					Type:     2,
					Currency: 2,
				})
				Ω(err).ShouldNot(BeNil())
			})
		})
	})

	Describe("GetWallet()", func() {
		Context("correct request", func() {
			It("should return correct wallet", func() {
				dbWallet := createWallet(testWalletUserId)
				defer deleteWallet(dbWallet.Id)

				protobufWallet, err := client.GetWallet(main.Ctx(), &protobufMain.ById{
					Id: dbWallet.Id,
				})
				Ω(err).Should(BeNil())

				compareWallet(protobufWallet, dbWallet)
			})
		})
	})

	Describe("GetWalletByUserAndType()", func() {
		Context("correct request", func() {
			It("should return correct wallet from db by user and type", func() {
				dbWallet := createWallet(testWalletUserId)
				dbWallet.Type = protobufWallet.WalletTypeDemo
				dbWallet.Save()

				defer deleteWallet(dbWallet.Id)

				protobufWallet, err := client.GetWalletByUserAndType(main.Ctx(), &protobufWallet.ByUserAndWalletType{
					User: testWalletUserId,
					Type: dbWallet.Type,
				})
				Ω(err).Should(BeNil())

				compareWallet(protobufWallet, dbWallet)
			})
		})
	})

	Describe("GetWalletsByUser()", func() {
		Context("correct request", func() {
			It("should return correct wallets from db by user", func() {
				dbWallet1 := createWallet(testWalletUserId)
				dbWallet1.Type = protobufWallet.WalletTypeDemo
				dbWallet1.Save()
				dbWallet2 := createWallet(testWalletUserId)
				dbWallet2.Type = protobufWallet.WalletTypeReal
				dbWallet2.Save()

				dbWallets := []*db.Wallet{dbWallet1, dbWallet2}

				protobufWalletsReply, err := client.GetWalletsByUser(main.Ctx(), &protobufMain.ByUser{
					User: testWalletUserId,
				})
				Ω(err).Should(BeNil())

				defer deleteWallets(protobufWalletsReply.Wallet)

				for i, protobufWallet := range protobufWalletsReply.Wallet {
					compareWallet(protobufWallet, dbWallets[i])
				}
			})
		})
	})
}
