create table wallet
(
  id bigserial not null
    constraint wallet_pkey
    primary key,
  currency integer not null,
  type integer not null,
  status integer default 1 not null,
  balance double precision default 0 not null CHECK (balance >= 0),
  locked timestamp,
  created timestamp default now() not null,
  updated timestamp default now() not null
)
;

create unique index wallet_id_uindex
  on wallet (id)
;

create unique index usertype
  on wallet (user_id, type)
;

create table "request"
(
  id bigserial not null
    constraint request_pkey
    primary key,
  wallet_id bigint not null
    constraint wallet_fk
    references wallet
    on delete cascade,
  purse_id bigint,
--     constraint purse_fk
--     references purse
--     on delete cascade,
  type integer not null,
  status integer not null,
  source integer not null,
  amount double precision not null,
  bonus double precision,
  turnover double precision,
  use_coupon boolean,
  coupon_id bigint,
  use_rate boolean,
  rate_id bigint,
  ga_cid varchar(255),
  ip_addr varchar(16),
  ip_country integer,
  gateway integer,
  created timestamp default now() not null,
  updated timestamp default now() not null
)
;

create unique index request_id_uindex
  on "request" (id)
;

comment on table "request" is 'Client wallet request'
;

create table transaction
(
  id bigserial not null
    constraint transaction_pkey
    primary key,
  wallet_id bigint not null
    constraint wallet_fk
    references wallet
    on delete cascade,
  type integer not null,
  target integer not null,
  direction integer not null,
  reference integer not null,
  reference_id varchar(255) not null,
  before double precision not null,
  amount double precision not null,
  after double precision not null,
  created timestamp default now() not null
)
;

create unique index transaction_id_uindex
  on transaction (id)
;

comment on table transaction is 'Client payment transaction'
;

create table bill
(
  id bigserial not null
    constraint bill_pkey
    primary key,
  request_id bigint not null
    constraint request_fk
    references "request"
    on delete cascade,
  type integer not null,
  status integer not null,
  amount double precision not null,
  gateway integer not null,
  reference_id varchar(255),
  problem integer,
  withdraw_id bigint,
  mask varchar(255),
  country integer,
  exchange_rate double precision,
  created timestamp default now() not null,
  updated timestamp default now() not null
)
;

create unique index bill_id_uindex
  on bill (id)
;

comment on table bill is 'Client request bill'
;

create table exchange_rate
(
  id bigserial not null
    constraint exchange_rate_pkey
    primary key,
  src smallint not null,
  dst smallint not null,
  value double precision not null,
  created timestamp not null
)
;